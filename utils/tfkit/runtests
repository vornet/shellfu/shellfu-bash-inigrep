#!/bin/bash
# tfkit - Shellfu's movable test framework
# See LICENSE file for copyright and license details.

TF_VERSION="0.0.16"

die() {
    echo "$@" && exit 9
}

usage() {
    echo "usage: $(basename "$0") [-c|-C] [-t tests_re] [-s subtest_re] [-p binpath] [-v] [-d]" >&2
    exit 2
}

version() {
    echo "TFKit (A trivial test kit) $TF_VERSION"
    exit 0
}

#
# Relic directory path
#
TF_RELICS="${TF_RELICS:-relics}"

#
# Relic collection mode
#
# 'always' to always collect, 'never` to never collect and 'auto'
# to collect only on failed tests.
#
TF_COLLECT="${TF_COLLECT:-auto}"

#
# Enable color?
#
TF_COLOR=${TF_COLOR:-true}

#
# Turn on debug mode?
#
TF_DEBUG="${TF_DEBUG:-false}"

#
# Location of own directory
#
TF_DIR=${TF_DIR:-$(dirname "$0")}

#
# Regex (BRE) to filter subtests based on name
#
TF_FILTER_SUBTEST=${TF_FILTER_SUBTEST:-}

#
# Regex (BRE) to filter tests based on name
#
TF_FILTER_TEST=${TF_FILTER_TEST:-}

#
# Location of test suite
#
TF_SUITE="${TF_SUITE:-tests}"

#
# Turn on verbosity?
#
TF_VERBOSE=${TF_VERBOSE:-true}


. "$TF_DIR/include/harness.sh" \
 || die "cannot import harness; is TF_DIR set properly?: $TF_DIR"

main() {
    while true; do case "$1" in
        -c|--collect)           TF_COLLECT=always;          shift ;;
        -C|--no-collect)        TF_COLLECT=never;           shift ;;
        -d|--debug)             TF_DEBUG=true;              shift ;;
        -p|--prefix)            export PATH="$(readlink -f "$2")/bin:$PATH"
                                                            shift 2 || usage ;;
        -s|--filter-subtest)    TF_FILTER_SUBTEST="$2";     shift 2 || usage ;;
        -t|--filter-test)       TF_FILTER_TEST="$2";        shift 2 || usage ;;
        -v|--verbose)           TF_VERBOSE=true;            shift ;;
        --version-semver)       echo "$TF_VERSION"; exit 0 ;;
        --version)              version ;;
        "") break ;;
        *)  usage ;;
    esac done
    export LC_ALL=C
    time tf_run_tests
}

main "$@"
